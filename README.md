# SolaGayosoEnriqueTorresHernandezJavierGrupo2Prac3

PRÁCTICA 02
Resolución con paso de mensajes
RESOLUCIÓN TEÓRICA
Primero de todo, resolveremos la práctica como hacemos en los ejercicios teóricos. Justificaremos adecuadamente las decisiones.
Dado que es la práctica 02, estaremos obligados a usar paso de mensajes y no otra herramienta para su resolución.
ANÁLISIS DE LA PRÁCTICA A REALIZAR
Se nos presenta un problema de renderizado de escenas, compuestas por fotogramas y con ejecución puramente concurrente. La granja de renderizado,
tiene dos tipos de procesos que gestionan dichas escenas:

Generadores: Son capaces de crear escenas de dos tipos de prioridad, alta y baja. Dichas escenas serán almacenadas en 2 listas de escenas
diferentes de tamaño limitado.
Renderizadores: Procesarán las escenas en orden de mayor a menor prioridad. Esto sugiere que las listas de escenas serán compartidas
por estos dos procesos.

Los elementos destacados son:


Fotogramas: Tienen un identificador único por cada fotograma y un tiempo de cálculo aleatorio entre MIN_TIEMPO_FOTOGRAMA y
MIN_TIEMPO_FOTOGRAMA + VARIACION_TIEMPO segundos.


Escenas: Tienen un identificador único para cada escena. Un atributo para controlar la prioridad, alta o baja, de la escena,
que indicará si se atenderán antes o después por los renderizadores. Solo cuando no haya escenas de prioridad alta, se atienden
las de prioridad baja. Por definición, una escena es un conjunto de fotogramas con tamaño aletorio entre MIN_NUM_FOTOGRAMAS y
MIN_NUM_FOTOGRAMAS + VARIACION_NUM_FOTOGRAMAS. El tiempo que se tarda en renderizar una escena, es la suma del tiempo de cada fotograma,
más un tiempo fijo llamado TIEMPO_FINALIZACION_ESCENA.


Generadores: Crean una escena y la ponen a disposición de los renderizadores. Un generador tarda un tiempo aleatorio entre
MIN_TIEMPO_GENERACION Y MIN_TIEMPO_GENERACION + VARIACION_TIEMPO segundos en generar la escena.


Renderizadores: Toma una escena de las que hay disponibles y la renderizan. Siempre que haya, elige primero de la cola de prioridad alta,
si no hay en esa cola, las coge de la de prioridad baja. Una escena no está completa hasta que no se han renderizado todos sus fotogramas.
Cuando una escena ha sido renderizada, se coloca en una lista de escenas resultado.


Los generadores de escenas están conectados con los renderizadores de escenas mediante dos listas de escenas con acceso compartido. Hay dos
listas según la prioridad. Los generadores escriben escenas en las listas y los renderizadores las leen. ( Problema clásico de “Productor y
consumidor” ). Esas listas tienen un tamaño máximo de MAX_ESCENAS_EN_ESPERA. Si ya hay MAX_ESCENAS_EN_ESPERA en una lista y un generador tiene
que escribir en ella, deberá esperar a que salga aluna escena antes de poder seguir escribiendo en la lista.
PROCESO GENERAL
Se construirán NUM_GENERADORES Generadores de escenas. Se construirán NUM_RENDERIZADORES Renderizadores de escenas. Cada Generador de escenas
construirá un número de escenas comprendido entre MIN_NUM_ESCENAS y MIN_NUM_ESCENAS + VARIACION_NUM_ESCENAS.
Cuando los generadores han generado todas sus escenas, terminan su ejecución. Cuando no quedan escenas en ninguna de las listas y los generadores
han terminado su ejecución, los renderizadores terminan su ejecución.
Al finalizar la ejecución de todas las tareas, el programa mostrará:
•	El número total de escenas procesadas.
•	El tiempo total empleado en Renderizar todas las escenas.
Para cada escena:

La hora en que se generó.
La hora en que un Renderizador comenzó su procesamiento.
Los fotogramas que la componen y la duración de cada uno.
La hora en la que el Renderizador terminó su procesamiento.
El tiempo total empleado en el procesamiento de la escena.

CLASES , MÉTODOS Y TIPOS DE DATOS ABSTRACTOS

CLASES

Fotograma : Clase para controlar los fotogramas generados.

•	ATRIBUTOS
o	IDFotograma: int.
o	Tiempo: int.
•	MÉTODOS O FUNCIONES
o	GetTiempoCalculo(): Devuelve el tiempo de cálculo del fotograma.

Escena : Clase para controlar los fotogramas generados.

•	ATRIBUTOS
o	IDEscena: int.
o	TiempoTotal: int.
o	Prioridad: Boolean. True = alta , False = baja.
o	BufferFotogramas: Buffer.
•	MÉTODOS O FUNCIONES
o	GeneraFotogramas(): Crea los fotogramas de la escena adecuada.
o	FinalizaFotograma(): Tiempo finalización fotogramas.

Generador : Clase para generar escenas.

•	ATRIBUTOS
o	Tiempo: int. Tiempo recopilación fotogramas.
•	MÉTODOS O FUNCIONES
o	GeneraEscenas(): Método principal. Genera todas las escenas.

Renderizador : Clase para procesar las escenas.

•	ATRIBUTOS
o	TiempoTotal: int. Tiempo total de procesar una escena.
•	MÉTODOS O FUNCIONES
o	ProcesaEscena(): Lee y procesa las escenas.

Buffers : Clase para controlar los buffer y las estructuras que los trabajan.

•	ATRIBUTOS
o	BufferAlta: Buffer. Buffer de escenas de alta prioridad.
o	BufferBaja: Buffer. Buffer de escenas de baja prioridad.

Principal : Clase principal que lanzará todos los recursos y controlará la ejecución y finalización del resto de procesos y clases.

•	ATRIBUTOS
o	Buffer: Buffers.
•	MÉTODOS O FUNCIONES
o	EjecutaGeneradores(). Crea los generadores de escenas y los lanza.
o	EjecutaRenderizadores(). Crea los renderizadores y los lanza.
o	FinalizaGeneradores(). Controla y finaliza los generadores lanzados anteriormente.
o	FinalizaRenderizadores(). Controla y finaliza los renderizadores lanzados anteriormente.

Controlador : Clase para controlar el paso de mensajes

•	ATRIBUTOS
o	Escena: Escena.
o	ID: int.
o	Fin: bool.
•	MÉTODOS O FUNCIONES
o	EjecucionControlador(): Método principal.

• TAD MailBox de < TipoDato >: buzon que almacena elementos del tipo
TipoDato

# BUZONES

Necesitaremos  buzones para el paso de mensajes entre Generadores y Controlador, y entre este y los Renderizadores y para
gestionar el funcionamiento de la práctica.

● EscenaAltaPrioridad : Mailbox de <Escena>. Buzón para el envío de Escenas de alta
prioridad desde los generadores.
● EscenaBajaPrioridad : Mailbox de <Escena>. Buzón para el envío de Escenas de baja
prioridad desde los generadores.
● termina : Mailbox de <logico>. Buzón para el envío de la señal de finalización
cuando todos los generadores han terminado con sus escenas.
● solicitud : Mailbox de <entero>. Buzón por donde los Renderizadores reciben
escenas para su renderizado.
● envío : Mailbox de <Escena>. Buzones que permiten el envío de Escenas
hasta los Renderizadores.
● terminado : Mailbox de <logico>. Buzón que permite que la señal de finalización sea recibida
por los Renderizadores para que finalicen al no quedar escenas por procesar.

ESPECIFICACIONES DE PROCEDIMIENTOS

En el análisis anterior de las clases y sus métodos, hemos sido escuetos en cuanto al funcionamiento de los distintos procesos. Como el último
paso para concluir nuestro informe de análisis y diseño es el de implementar el pseudocódigo de la práctica, es conveniente que previamente
entremos un poco más en profundidad en el funcionamiento de estos métodos y procesos, hablando de ellos como procesos. Además también mencionaremos
aspectos no tratados anteriormente.
En el proceso Principal (hilo principal):
	CrearProceso( Generadores, Procesadores ): Creará los procesos Generador y Procesador.
	EjecutarProceso( Generadores, Procesadores ): Ejecutará los procesos correspondientes y los lanzará.
	EsperarProceso( Escenas, Procesadores): Esperará a que todos los fotogramas de todas las escenas correspondientes hayan sido finalmente procesados.
	FinalizarProceso( Generadores, Procesadores): Finalizará todos los procesos del sistema.
En el proceso Escena:
	GeneraFotogramas(): crea un número aleatorio de fotogramas entre MIN_NUM_FOTOGRAMAS y MIN_NUM_FOTOGRAMAS + VARIACION_NUM_FOTOGRAMAS que representa los fotogramas que forman la Escena final.
	FinalizaFotogramas(): permite indicar el tiempo de finalización de fotogramas solicitados.
En el proceso Generador:
	GeneraEscenas(): permite crear un número de escenas entre MIN_NUM_ESCENAS y MIN_NUM_ESCENAS + VARIACION_NUM_ESCENAS.
	GeneraEscena(): genera una escena como tal y la agrega al buffer que corresponda.
En el proceso Procesador:
	ProcesaEscena(): Procesa una escena de la buffer correspondiente según prioridades.
En el proceso Finalizador:
	MostrarDatos( Resultados ): Este procedimiento presenta de forma legible los datos solicitados.
En el proceso Controlador:
 inicializaBroker(): configura el Broker y lo ejecuta.
 ejecucionControlador(). Gestiona el acceso a los buzones por parte de los procesos
 finalizaBroker (): Corta conexión con el Broker. 

DISEÑO Y PSEUDOCÓDIGO

Main
{
    crearProceso(Generadores)
    ejecucion_proceso(Generadores)
    while ( esperarProceso(Generadores) ) { }
    finalizarProceso(Generadores)
}

Renderizador
{

    crearProceso(Renderizadores)
    ejecucion_proceso(Renderizadores)
    while ( esperarProceso(Renderizadores) ) { }
    finalizarProceso(Renderizadores)
    
}

Fotograma
{
    int id;
    int tiempo;

    getTiempo(): int 
    {
       return tiempo;
    }
}

Escena
{

    int id;
    boolean prioridad;
    int tiempo_total;
    Lis<Fotogramas> BufferFtgrma;
    Semaphore EXCMBuffer;
    
ejecucion()
{
    // genera fotogramas
    for(int i = 0; i< num_fotogramas; i++)
    {
        Fotograma f=generaFotograma_aleatorio(i)
        tiempoTotal+=simulaFotograma_tiempo() //sumo tiempos
        bufferFotograma.add(fotograma)
    }
}
}

Generador
{
    int tiempo;
    Lista<Escena> resultado;

Lista<Escena> Ejecucion()
{
    // genera escenas y las manda
    for(int i = 0; i< num_escenas; i++)
    {
        Escena escena = generaEscena()
        resultado.add(escena)
        if (escena.prioridad() == true )
        {
            send (envioEscenaPrioridadAlta, escena)
        }else
        {
            send (envioEscenaPrioridadBaja, escena)
        }
        barrera.espera()
        send (finaliza, true)
        devolver resultado
    }
}
}

Renderizador
{

    int tiempo;
    Lista<Escena> resultado;
    boolean terminado;
    
Lista<Escena> Ejecucion()
{
    while(!terminado)
    {
        inicio(now());
        consultaGeneradores()
        
        if(!terminado)
        {
            send(solicitaEscena, id)
            receive(envioEscena, escena)
            Sleep( Tiempo_redner+tiempoFinEscena );
            fin(now())
            resultado.add(escena)
            
        }else
        {
            devolver resultado
        }
    }
}

consultasGeneradores() 
{
    receive (fin, terminado)
}
}

Controlador
{

    Escena escena;
    int id;
    int fin;
    boolean terminado;
    Lista<Escena> PrioridadAlta;
    Lista<Escena> PrioridadBaja;
    Lista<Integer> Peticiones: 
    Lista<Escena> ListaNoInsertadaPrioridadAlta: 
    Lista<Escena> ListaNoInsertadaPrioridadBaja:
    
ejecucion()
{
    inicializaBroker();
    terminado = false;
    
    while ( fin ) 
    {
        select;
        
        if(!listaPrioridadAlta.llena())
        {
            CargarEscenaPrioridadAlta()
        }
        else
        {
            listaNoInsertadaPrioridadAlta.add()
        }

        or
        
        if(!listaPrioridadBaja.llena())
        {
            CargarEscenaPrioridadBaja();
        }
        else
        {
            listaNoInsertadaPrioridadBaja.add();
        }

        or
        
        Finaliza();
        
        or
        
        if( terminadosGeneradores )
        {
            ComunicaFinGeneradores()
        }
        
        or
        
        PeticionEscena ();
        
        or
        
        EnviarEscena ();
        
        or
        
        if(!listaNoInsertadaPrioridadAlta.vacia)
        {
            if(!listaAltaPrioridad.llena())
            {
                CargarEscenaPrioridadAlta();
            }
        }
        
        or
        
        if(!listaNoInsertadaPrioridadBaja.vacia)
        {
            if(!listaPrioridadBaja.llena())
            {
                CargarEscenaPrioridadBaja();
            }
        }

        }
    }
        finalizaBroker();
}

CargarEscenaAltaPrioridad() 
{
    receive (envioEscenaPrioridadAlta, escena);
    listaPrioridadAlta.add(escena);
}

CargarEscenaBajaPrioridad() 
{
    receive (envioEscenaPrioridadBaja, escena);
    listaPrioridadBaja.add(escena);
}


PeticionEscena () 
{
    receive(solicitaEscena, id)
    listaPeticiones.add(id)
}

Escena SacarEscena()
{
    if(!listaPrioridadAlta.vacia() && !peticiones.vacia() )
    {
        escena = listaPrioridadAlta.get();
        send(envioEscenaR[peticiones.get()], escena);
    }
    else
    {
        if(!listaPrioridadBaja.vacia() && !peticiones.vacia() )
        {
            escena = listaPrioridadBaja.get();
            send (envioEscenaR[ peticiones.get()], escena);
        }
    }

}

Finaliza () 
{
    receive (finaliza, terminado);
}

ComunicaFinGeneradores() 
{
    send (fin, terminado);
} 
}