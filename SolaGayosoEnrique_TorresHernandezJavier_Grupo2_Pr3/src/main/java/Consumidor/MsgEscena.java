
package Consumidor;

import Productor.Escena;


public class MsgEscena {
    private Escena nombresito;
    private String referencia;

    public MsgEscena(Escena e, String r) {
        this.nombresito=e;
        this.referencia=r;
    }

    public Escena getNombresito() {
        return nombresito;
    }


    public String getReferencia() {
        return referencia;
    }
    
    

    
}
