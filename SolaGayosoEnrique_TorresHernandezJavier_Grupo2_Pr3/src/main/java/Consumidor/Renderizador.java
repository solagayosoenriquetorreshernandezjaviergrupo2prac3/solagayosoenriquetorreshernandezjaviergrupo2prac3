
package Consumidor;

import Interfaz.Controlador;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.Semaphore;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;

import static Interfaz.Constantes.BROKER_URL;
import static Interfaz.Constantes.QUEUE;
import javax.jms.Connection;
import javax.jms.Destination;
import javax.jms.MessageConsumer;
import javax.jms.MessageProducer;
import javax.jms.Session;
import javax.jms.TextMessage;
import org.apache.activemq.ActiveMQConnectionFactory;
import Interfaz.GsonUtil;
import Productor.Escena;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageListener;


public class Renderizador implements Callable<List<Escena>>{
    
    public static final int RETRASO = 1;
    //CADENA IDENTIFICADOR
    static int ident=0;
    private String cadena;
    //FINALIZADOR
    private static boolean terminadosGeneradores;
    List<Escena> resultado;
    
    //NUEVO
    private static Semaphore EXCM;
    
    //Semaphore mutex;
    //Monitor monitor
    private ActiveMQConnectionFactory Factory;
    private Connection conexion;
    private Session sesion;
    private Destination recibido;
    private Destination solicitud;
    private Destination terminado;
  
   // Controlador control;
    
   // boolean escenaProcesada;
    

    
  public Renderizador()
  {
    cadena=""+ident++;  
    terminadosGeneradores= false; 
    
    this.resultado=new LinkedList<>();

    EXCM = new Semaphore(1);
  }

  public void Preparacion() throws Exception {
        Factory = new ActiveMQConnectionFactory(BROKER_URL);
        conexion = Factory.createConnection();
        sesion = conexion.createSession(false, Session.AUTO_ACKNOWLEDGE);
      
        recibido = sesion.createQueue(QUEUE+"recibido");
        
        solicitud = sesion.createQueue(QUEUE+"solicitud");
        
        terminado = sesion.createQueue(QUEUE+"terminado");
               
    }
    
    public void Finalizacion() {
        try {
            conexion.close();
        } catch (Exception ex) {
            ///
        }    
    }


    // TODO code application logic here
       @Override
   public List<Escena> call() throws Exception  {
       
        System.out.println("Renderizador "+cadena + "Empieza");
        
        try {
            Preparacion();
            resultado = Ejecucion();
        } catch (Exception e) {
                ///
        } finally {
            Finalizacion();
            System.out.println("Renderizador "+cadena + "Termina");
            
        }
        return resultado;
    } 
 
   public List<Escena> Ejecucion() throws Exception {
            
    TextMessage contenido;
    MessageProducer creador_mensaje;

    Message mensaje;
   
    Escena escena;   
    
    MessageConsumer consumidor = sesion.createConsumer(recibido);                   
    TextMsgListener espera =new TextMsgListener("recibido");        
    consumidor.setMessageListener(espera);
    
    MessageConsumer consumer_terminado = sesion.createConsumer(terminado);                   
    TextMsgListener espera_terminado =new TextMsgListener("terminado");        
    consumer_terminado.setMessageListener(espera_terminado);
    
    conexion.start();
       
    while (true)
    {       
        
        if(terminadosGeneradores) {  
            return resultado;
        }
        
        EXCM.acquire();
        
        creador_mensaje = sesion.createProducer(solicitud);
        mensaje = sesion.createTextMessage(cadena);
        
        System.out.println(" --- El Renderizador "+cadena+ " pide una escena");
        
        creador_mensaje.send(mensaje);
        creador_mensaje.close();            
       
    }                      
     
  }
   
   
 class TextMsgListener implements MessageListener 
 {
    private final String nombre;

	
  public TextMsgListener(String consumerName) {
        this.nombre = consumerName;
        
  }

  //Todo esto se encarga de traducir los datos en el envio de mensajes
    @Override
    public void onMessage(Message message) {
        
	try {			
            if (message instanceof TextMessage) 
            {		
                TextMessage sms = (TextMessage) message;
                
                GsonUtil<MsgEscena> utilidad = new GsonUtil();
                
                if (nombre.equalsIgnoreCase("envioR"))
                {
                    MsgEscena e=null;
                    e = utilidad.decode(sms.getText(), MsgEscena.class);
                    Escena e_2 = e.getNombresito();
                    
                    System.out.println("Escena "+e_2.getCadena()+" como "+ e_2.getPrioridad()+ " en "+e.getReferencia());
                    
                    e_2.setInicio(new Date());
                    TimeUnit.SECONDS.sleep(e_2.getDuracion());
                    e_2.setFin(new Date());
                    resultado.add(e_2);

                }else
                {
               
                    terminadosGeneradores= true;              
                }              
                EXCM.release();
            } else
                System.out.println("Algo malo está pasando");
	
        } catch (JMSException e) {			
            e.printStackTrace();
	} catch (InterruptedException ex) {	
            Logger.getLogger(Controlador.class.getName()).log(Level.SEVERE, null, ex);
        }	
    }
  }
  
 
}
