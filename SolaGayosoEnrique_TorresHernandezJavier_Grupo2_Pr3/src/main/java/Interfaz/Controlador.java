package Interfaz;

import java.util.List;

import java.util.LinkedList;
import javax.jms.TextMessage;
import java.util.concurrent.Semaphore;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.jms.Destination;
import javax.jms.MessageConsumer;
import javax.jms.Session;
import Consumidor.MsgEscena;
import static Interfaz.Constantes.BROKER_URL;
import static Interfaz.Constantes.MAX_ESCENAS_EN_ESPERA;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.MessageProducer;
import javax.jms.Connection;
import org.apache.activemq.ActiveMQConnectionFactory;
import static Interfaz.Constantes.QUEUE;
import Interfaz.GsonUtil;
import Productor.Escena;
import javax.jms.JMSException;


public class Controlador implements Runnable{
    
    private boolean fin_generar;

    private final List<String> Solicitudes;
    private final List<Escena> AltaPrioridad;
    private final List<Escena> BajaPrioridad;
    
    private Semaphore LlenoAltaPrioridad;
    private Semaphore VacioAltaPrioridad;
    private List<Escena> listaAltaPrioridad;
    
    private Semaphore LlenoBajaPrioridad; 
    private Semaphore VacioBajaPrioridad;
    private List<Escena> listaBajaPrioridad;
    
    private Semaphore EXCMAlta;
    private Semaphore EXCMBaja;
    private Semaphore EXCM_Principal;

    private ActiveMQConnectionFactory Factory;
    private Connection conexion;
    private Session sesion;
    private Destination[] destino;
    private Destination fin;
    
    
    private Destination enviado;
    private Destination recibido;
    private Destination terminado;
         
  public Controlador(int max_escenas){
        this.fin_generar=false; 
        this. listaBajaPrioridad= new LinkedList<>();
        this.destino = new Destination[2];
        this.listaAltaPrioridad= new LinkedList<>();
        this.Solicitudes= new LinkedList<>();
        this.AltaPrioridad= new LinkedList<>();
        this.BajaPrioridad= new LinkedList<>();
  }
  
    public void Preparacion() throws Exception {
        Factory = new ActiveMQConnectionFactory(BROKER_URL);
        conexion = Factory.createConnection();        
        sesion = conexion.createSession(false, Session.AUTO_ACKNOWLEDGE);
        destino[0] = sesion.createQueue(QUEUE+"Alta");
        destino[1] = sesion.createQueue(QUEUE+"Baja");
        fin = sesion.createQueue(QUEUE+"Terminado");
        enviado = sesion.createQueue(QUEUE+"enviado");
        recibido = sesion.createQueue(QUEUE+"Recibido");
        terminado = sesion.createQueue(QUEUE+"terminado");
    }

    public void Finalizacion() {
        try {
            conexion.close();
        } catch (JMSException ex) {
            ///
        }    
    }
    

    @Override
  public void run() {
        System.out.println("Controlador empieza");
        try {
            Preparacion();
            Ejecucion();
        } catch (Exception e) {
            ///
        } finally {
            Finalizacion();
        }
        System.out.println("Controlador termina");
    }
      
    
    @SuppressWarnings("empty-statement")
  public void Ejecucion() throws Exception {
      
        consultaTerminadosGeneradores();
        MessageConsumer consumer = sesion.createConsumer(destino[0]);            
        TextMsgListener listener1 =new TextMsgListener("R_Alta");
        consumer.setMessageListener(listener1);
        
        MessageConsumer consumer2 = sesion.createConsumer(destino[1]);            
        TextMsgListener listener2 =new TextMsgListener("R_Baja");
        consumer2.setMessageListener(listener2);
        
        
        MessageConsumer consumerR1 = sesion.createConsumer(enviado);            
        TextMsgListener listenerRl =new TextMsgListener("enviado");
        consumerR1.setMessageListener(listenerRl);

        
        conexion.start();
        
        while(!Thread.interrupted());
      
  }
  
  public boolean getTerminadores(){
      return fin_generar;
  }
  
  public void consultaTerminadosGeneradores() throws JMSException{
            
       MessageConsumer consumer = sesion.createConsumer(fin);
       TextMsgListener listener0 =new TextMsgListener("Finalizador");
       consumer.setMessageListener(listener0);
      
  }
  

  class TextMsgListener implements MessageListener {
    private String consumerName;
    private String tipo;
	
    public TextMsgListener(String consumerName) {
        this.consumerName = consumerName;
        
    }

    @Override
    public void onMessage(Message message) {
        Escena e=null;
        String r;
	try {			
            if (message instanceof TextMessage) {		
                TextMessage textMessage = (TextMessage) message;
                GsonUtil<Escena> gsonUtil = new GsonUtil();
                if (consumerName.equalsIgnoreCase("RecibeAlta")){ 
                    e = gsonUtil.decode(textMessage.getText(), Escena.class);
                   if (listaAltaPrioridad.size()<MAX_ESCENAS_EN_ESPERA){                        
                        System.out.println("Inserta escena "+e.getCadena()+" de Alta prioridad");
                        CargarEscenaAltaPrioridad(e);
                   }else
                       AltaPrioridad.add(e);
                    
                }else{ 
                if (consumerName.equalsIgnoreCase("RecibeBaja")){ 
                   e = gsonUtil.decode(textMessage.getText(), Escena.class);
                    if (listaBajaPrioridad.size()<MAX_ESCENAS_EN_ESPERA){                                           
                        System.out.println("Inserta escena "+e.getCadena()+" de Baja prioridad");
                        CargarEscenaBajaPrioridad(e);
                    }else
                       BajaPrioridad.add(e);

               }else{
                if (consumerName.equalsIgnoreCase("PeticionR")){                
                    r = textMessage.getText();
                    Solicitudes.add(r);
                   
                }else{ 
                    if (consumerName.equalsIgnoreCase("Finalizador")){
                        System.out.println(consumerName + " ejecutandose.... estado: " + textMessage.getText());
                       
                        fin_generar=true;
                    }
                     
                 }
              }
            }
            if (!Solicitudes.isEmpty()){
                if (!listaAltaPrioridad.isEmpty()){
                    r= Solicitudes.remove(0);                      
                    DevolverEscena(r,0);

                } else{
                    if (!listaBajaPrioridad.isEmpty()){
                        r= Solicitudes.get(0);
                        Solicitudes.remove(0);
                        DevolverEscena(r,1);

                    }                        
                }
            }
            
            if (fin_generar && listaBajaPrioridad.isEmpty() && listaAltaPrioridad.isEmpty() &&
                    AltaPrioridad.isEmpty() && BajaPrioridad.isEmpty()  ){             
                ComunicarFin();
            }
            if ( !AltaPrioridad.isEmpty() && listaAltaPrioridad.size()<MAX_ESCENAS_EN_ESPERA
                    && consumerName.equalsIgnoreCase("PeticionR")){                
                e= AltaPrioridad.remove(0);
                System.out.println("Inserta escena "+e.getCadena()+" de Alta prioridad");
                CargarEscenaAltaPrioridad(e);
            }
            
            if ( !BajaPrioridad.isEmpty() && listaBajaPrioridad.size()<MAX_ESCENAS_EN_ESPERA
                    && consumerName.equalsIgnoreCase("PeticionR")){                
                e= BajaPrioridad.remove(0);
                System.out.println("Inserta escena "+e.getCadena()+" de Baja prioridad");
                CargarEscenaBajaPrioridad(e);
            }
            
        } else
                System.out.println("Unknown message");
	} catch (JMSException ej) {			
            ej.printStackTrace();
	} catch (InterruptedException ex) {	
            Logger.getLogger(Controlador.class.getName()).log(Level.SEVERE, null, ex);
        }	
   
    }
    }
  
    public void DevolverEscena( String r, int tipo) throws InterruptedException, JMSException{
        MessageProducer producer;
        TextMessage msg;
        Escena e=null;
        GsonUtil<MsgEscena> gsonUtil = new GsonUtil();
        
        if (tipo==0)
            e=SacarEscenaA();
        else
            e=SacarEscenaB();
        
        MsgEscena esc = new MsgEscena(e,r);
        producer = sesion.createProducer(recibido);
        msg = sesion.createTextMessage(gsonUtil.encode(esc, MsgEscena.class));
        System.out.println("Mandamos escena "+e.getCadena()+" de Prioridad "+ e.getPrioridad()+ " al Renderizador "+r);
        producer.send(msg);
        producer.close();
     
    }
    
    public void ComunicarFin() throws InterruptedException, JMSException{
        MessageProducer producer;
        TextMessage msg;
        
        // Creamos la instancia del objeto para enviar el mensaje
        GsonUtil<Boolean> gsonUtilFin = new GsonUtil();
        
        producer = sesion.createProducer(terminado);
        msg = sesion.createTextMessage(gsonUtilFin.encode(true, Boolean.class));
        producer.send(msg);
        producer.close();   
    }

    public void CargarEscenaAltaPrioridad(Escena escena) throws InterruptedException{      

        try {            
            listaAltaPrioridad.add(escena);
        } finally {
        }
    }
    
    public void CargarEscenaBajaPrioridad(Escena escena) throws InterruptedException{

        try {            
            listaBajaPrioridad.add(escena);
        } finally {    
   
        }
    }
    

    
    public Escena SacarEscenaA() throws InterruptedException{
        Escena escena=null;     
        

        try{ 
            escena= listaAltaPrioridad.get(0);
            listaAltaPrioridad.remove(0);
        } finally {

        }
        
        return escena;
    }
    

    
     public Escena SacarEscenaB() throws InterruptedException{
        Escena escena=null;

        try{ 
            escena= listaBajaPrioridad.get(0); 
            listaBajaPrioridad.remove(0);
        } finally {
        
        }
        return escena;
    }
         
  
}
