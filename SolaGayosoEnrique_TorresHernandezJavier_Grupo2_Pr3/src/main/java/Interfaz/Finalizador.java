package Interfaz;

import javax.jms.Connection;
import javax.jms.Destination;
import javax.jms.JMSException;
import javax.jms.MessageProducer;
import javax.jms.Session;
import javax.jms.TextMessage;
import org.apache.activemq.ActiveMQConnectionFactory;

import static Interfaz.Constantes.BROKER_URL;
import static Interfaz.Constantes.QUEUE;


public class Finalizador implements Runnable{
    //LÍNEA
    private ActiveMQConnectionFactory Factory;
    
    //AJUSTES AMQ
    private Session sesion;
    private Destination destino;
    private Connection conexion;

    public void preparacion() throws Exception 
    {
        //Establecemos la conexión a traves de la URL dada
        Factory = new ActiveMQConnectionFactory(BROKER_URL);
        
        //Activamos la conexion
        conexion = Factory.createConnection();
        //Iniciamos la sesion con el autoreconocimiento (correindo el AMQ por debajo )
        sesion = conexion.createSession(false, Session.AUTO_ACKNOWLEDGE);
        //Preparamos el destino al que lanzar el creador_mensajes
        destino = sesion.createQueue(QUEUE+"End");
        //Iniciamos la conexión
        conexion.start();
    }
    
    public void finalizacion() 
    {
        //Simplemente cerramos la conexión pero controlando las excepciones
        try 
        {
            conexion.close();
        } catch (JMSException e) {
                ///
        }    
    }

    @Override
    public void run(){

        System.out.println("Finalizador empezando");        
        try {
            preparacion();            
            ejecutar();
        } catch (Exception e) 
        {
            ///
        } finally {
            finalizacion();
            System.out.println("Finalizador terminando");            
        }
    }
       
    public void ejecutar() throws Exception {
           
        TextMessage contenido;
        MessageProducer creador_mensajes;

        // Lanzamos el creador de mensajes gracias al a siguiente instancia
        GsonUtil<Boolean> gsonUtilFin = new GsonUtil();

        //Creamos el mensaje y nos preparamos para su envío
        contenido = sesion.createTextMessage(gsonUtilFin.encode(true, Boolean.class));
        
        //Envío del mensaje
        creador_mensajes = sesion.createProducer(destino);
        creador_mensajes.send(contenido);
        creador_mensajes.close();
           
    }    
}
