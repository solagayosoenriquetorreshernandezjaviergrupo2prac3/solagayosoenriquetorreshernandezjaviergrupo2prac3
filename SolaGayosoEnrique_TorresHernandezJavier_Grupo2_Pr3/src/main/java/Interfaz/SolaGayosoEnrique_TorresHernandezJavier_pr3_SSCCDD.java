package Interfaz;
import static Interfaz.Constantes.NUM_GENERADORES;
import java.util.concurrent.BrokenBarrierException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeoutException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.ExecutionException;
import static Interfaz.Constantes.MAX_ESCENAS_EN_ESPERA;
import java.util.concurrent.Callable;
import java.util.concurrent.CyclicBarrier;
import java.util.concurrent.TimeUnit;
import static Interfaz.Constantes.NUM_RENDERIZADORES;
import Consumidor.*;
import Productor.*;
        

public class SolaGayosoEnrique_TorresHernandezJavier_pr3_SSCCDD {

    public static void main(String[] args) throws ExecutionException, BrokenBarrierException, InterruptedException, TimeoutException {

        
        System.out.println("Hilo(PRINCIPAL): Ha iniciado la ejecucion");

        ExecutorService ejecucion = (ExecutorService) Executors.newCachedThreadPool();              

        Finalizador finalizador= new Finalizador();  
        CyclicBarrier barrera= new CyclicBarrier(NUM_GENERADORES, finalizador);

        ArrayList<Callable<List<Escena>>> tareas = new ArrayList<>();
        
        for (int i=0; i<NUM_GENERADORES; i++) {
            Generador genera = new Generador( barrera);
            tareas.add(genera);
        }
        
        Controlador control= new Controlador(MAX_ESCENAS_EN_ESPERA);
        Future<?> future= ejecucion.submit(control);
        
        for (int i=0; i<NUM_RENDERIZADORES; i++) {
            Renderizador renderizador = new Renderizador( );
            tareas.add(renderizador);
        }

        List<Future<List<Escena>>> escenasFin = null;

        escenasFin = ejecucion.invokeAll(tareas);              
        
        float total=0f;
        int totalEscenas=0;
        for (int i=NUM_GENERADORES; i<NUM_GENERADORES+NUM_RENDERIZADORES; i++) {

            Future<List<Escena>> resultado = escenasFin.get(i);
             List<Escena> lista=resultado.get();
             totalEscenas+=lista.size();
             for(Escena datos:lista){
                 total+=datos.TotalProcesamiento();
                 System.out.println("La escena "+datos.getCadena()+" generó los siguientes datos :"+datos.toString());
             }
        }
        System.out.println("TotalEscenas: "+ totalEscenas);
        System.out.println("TotalProcesamiento: "+ total );

        future.cancel(true);
        ejecucion.shutdown();
        ejecucion.awaitTermination(10, TimeUnit.SECONDS);
        System.out.println("Finalizaron todos los procesos");        

        System.out.println("\n** Hilo(PRINCIPAL): Ha finalizado la ejecucion");
        
    }
    
}
