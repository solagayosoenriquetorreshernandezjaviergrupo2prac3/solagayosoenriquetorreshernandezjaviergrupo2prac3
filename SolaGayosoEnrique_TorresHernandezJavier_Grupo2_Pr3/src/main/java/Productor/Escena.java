/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Productor;

import static Interfaz.Constantes.MIN_NUM_FOTOGRAMAS;
import static Interfaz.Constantes.TIEMPO_FINALIZACION_ESCENA;
import static Interfaz.Constantes.VARIACION_NUM_FOTOGRAMAS;
import static Interfaz.Constantes.generador;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;


public final class Escena { 
    
    //TIEMPOS DE CONTROL DE FUTURE Y FINALIZADOR
    private Date tiempo;
    private Date inicio;
    private Date fin;
    
    //IDENTIFICADOR
    static int ident=0;
    //CADENA IDENTIFICADOR
    private final String cadena;
    //TIEMPO DE DURACIÓN
    private int duracion;
    //PRIORIDAD
    private boolean prioridad;
    
    //RECURSO COMPARTIDO
    private List<Fotograma> listaFotogramas;    
    
    boolean prioridad(int n)
    {
        return n==0;
    }
    
    public Escena() throws InterruptedException{   
        
        inicio= new Date();
        fin= new Date();
        
        ident = ident + 1;
        cadena = ">" + ident + "<";       
        duracion = TIEMPO_FINALIZACION_ESCENA;
        int tipo = generador.nextInt(2);
        prioridad = prioridad(tipo);
        
        listaFotogramas = new LinkedList<>();        
                
   } 
   
   public void inicializaEscena()
   {
       setTiempo(new Date());
       
       int numFotogramasAGenerar;
       numFotogramasAGenerar= MIN_NUM_FOTOGRAMAS+generador.nextInt(VARIACION_NUM_FOTOGRAMAS);        
       for (int foto=1; foto<=numFotogramasAGenerar; foto++)
       {
           Fotograma f = new Fotograma();
           int add = f.getDuracion();
           int total = this.getDuracion() + add;
           setDuracion(total);
           listaFotogramas.add(f);
       }
   }

    public String getCadena()
    {return cadena;}

    public int getDuracion()
    {return duracion;}

    public void setDuracion(int _duracion)
    {duracion = _duracion;}

    public boolean getPrioridad()
    {return prioridad;}

    public void setPrioridad(boolean _prioridad)
    {prioridad = _prioridad;}

    public Date getTiempo()
    {return tiempo;}

    public void setTiempo(Date _tiempo)
    {tiempo = _tiempo;}

    public Date getInicio()
    {return inicio;}

    public void setInicio(Date _inicio)
    {inicio = _inicio;}

    public Date getFin()
    {return fin;}

    public void setFin(Date _fin)
    {fin = _fin;}

    public List<Fotograma> getListaFotogramas()
    {return listaFotogramas;}

    public void setListaFotogramas(List<Fotograma> _listaFotogramas)
    {listaFotogramas = _listaFotogramas;}
    
    public float TotalProcesamiento()
    {
        long _final = fin.getTime();
        long _inicio = inicio.getTime();
        return (_final - _inicio)/1000; //AJUSTAMOS EN TIEMPO REAL
    }
    @Override
    public String toString(){
        String cadena_salida;
        cadena_salida= "La escena " + cadena + "generada a las: " + tiempo +"\n" + "Se comienza a renderizar a las : " + inicio +  " y termina de renderizar a las : " + fin  +"\n" + " Tiempo total trabajando la escena: " + TotalProcesamiento() + "\n" + "Fotogramas trabajados en esta escena: ";
        for (Fotograma foto:listaFotogramas){
            cadena_salida=cadena_salida+ " >>>>>>>>> " +foto.toString() ;
        }
        cadena_salida=cadena_salida+"\n";
        
        return cadena_salida;
    }

}
