/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Productor;
//import Productor.Generador;

import static Interfaz.Constantes.INC_TIEMPO_FOTOGRAMA;
import static Interfaz.Constantes.MIN_TIEMPO_FOTOGRAMA;
import static Interfaz.Constantes.generador;


public class Fotograma {
    //IDENTIFICADOR
    static int ident=0;
    //CADENA DE IDENTIFICADOR
    private final String cadena;
    //TIEMPO DE DURACIÓN
    private int duracion;
    
    final int incremento()
    {return generador.nextInt(INC_TIEMPO_FOTOGRAMA);}
    
    public Fotograma()
    {
        ident = ident + 1;
        cadena=">" + ident + "<";
        int min = MIN_TIEMPO_FOTOGRAMA;
        duracion= min + incremento();  
    }

    public String getCadena()
    {return cadena;}

    public int getDuracion(){return duracion;}

    public void setDuracion(int _duracion)
    {duracion = _duracion;}
    
    @Override
     public String toString()
     {
         return "El fotograma: " + cadena + " ha trabajado: " + duracion;
     }
}
