package Productor;

import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.TimeUnit;
import static Interfaz.Constantes.MIN_TIEMPO_GENERACION;
import static Interfaz.Constantes.BROKER_URL;
import static Interfaz.Constantes.generador;
import Interfaz.GsonUtil;
import java.util.logging.Level;
import java.util.logging.Logger;
import static Interfaz.Constantes.VARIACION_NUM_ESCENAS;
import static Interfaz.Constantes.MIN_NUM_ESCENAS;
import javax.jms.MessageProducer;
import javax.jms.Session;
import javax.jms.TextMessage;
import org.apache.activemq.ActiveMQConnectionFactory;
import javax.jms.Connection;
import static Interfaz.Constantes.QUEUE;
import static Interfaz.Constantes.VARIACION_TIEMPO_GENERACION;
import javax.jms.Destination;
import javax.jms.JMSException;
import java.util.concurrent.Callable;
import java.util.concurrent.CyclicBarrier;

public class Generador implements Callable<List<Escena>>{
    
    //IDENTIFICADOR
    static int ident=0;    
    //CADENA DE IDENTIFICADOR
    String cadena;
    
    //LISTAS COMPARTIDAS
    List<Escena> resultado;
//    List<Escena> listaAltaPrioridad;
//    List<Escena> listaBajaPrioridad;
    
//    //SEMÁFOROS ( DUDA DE SI VAN AQUÍ O EN EL MAIN O SE PASAN POR CABECERA O QUE)
//    Semaphore LlenarAltaPrioridad;
//    Semaphore EXCMAltaPrioridad;
//    Semaphore VaciarAltaPrioridad;
//    Semaphore LlenarBajaPrioridad;
//    Semaphore EXCMBajaPrioridad;
//    Semaphore VaciarBajaPrioridad;
    
    //FINALIZADOR
    //Finalizador finalizador;
    
    //CONTROL DEL MARCO EJECUTOR
    CyclicBarrier barrier;
    
    private ActiveMQConnectionFactory Factory;
    private Connection conexion;
    private Session sesion;
    private Destination[] destinos;
  //  private Destination finalizador;
  
    
public Generador( CyclicBarrier barrera ){   //NUEVO
   
    ident++;
    cadena=">" + ident + "<";
    
    resultado=new LinkedList<>();
    
//    this.listaAltaPrioridad= listaAltaPrioridad; //CONTROL DEL ACCESO A LA LISTA
//    this.listaBajaPrioridad= listaBajaPrioridad; //SAME
//    
//    this.LlenarAltaPrioridad= LlenarAltaPrioridad; //ESTAMOS VACIOS EN ALTA PRIORIDAD, A LLENAR
//    EXCMAltaPrioridad= new Semaphore(1); //ACCESO O NO ACCESO
//    this.VaciarAltaPrioridad = VaciarAltaPrioridad; //ESTAMOS LLENOS EN ALTA PRIORIDAD, A VACIAR
//      
//    this.LlenarBajaPrioridad= LlenarBajaPrioridad; //IDEM
//    EXCMBajaPrioridad= new Semaphore(1); //ACCESO O NO ACCESO
//    this.VaciarBajaPrioridad = VaciarBajaPrioridad; //IDEM
    
    barrier= barrera; //MARCO EJECUTOR, CONTROL DE BARRERA Y FINALIZADOR
//    finalizador = fin;
    
    //PRACTICA 2 - MONITOR
//    monitor = m;
    
    this.destinos = new Destination[2];

   
}

 public void preparacion() throws Exception {
     
        //Mismo proceso para generar los mensajes pero ésta vez con las prioridades
        Factory = new ActiveMQConnectionFactory(BROKER_URL);
        conexion = Factory.createConnection();
        sesion = conexion.createSession(false, Session.AUTO_ACKNOWLEDGE);
      
        destinos[0] = sesion.createQueue(QUEUE+"Alta");
      
        destinos[1] = sesion.createQueue(QUEUE+"Baja");

        
        conexion.start();
    }
    
    public void finalizacion() {
        try {
            conexion.close();
        } catch (JMSException ex) {
            ///
        }    
    }

    @Override
   public List<Escena> call() throws Exception  {
       
        System.out.println("Generador "+cadena + " Empieza.");
        
        try {
            preparacion();
            resultado = ejecucion();
        } catch (Exception e) {
            ///
        } finally {
            finalizacion();
            System.out.println("Generador "+cadena + " Termina.");
            
        }
        return resultado;
    } 
       
       
    public List<Escena> ejecucion() throws Exception {
         
        TextMessage contenido;
        MessageProducer creador_mensajes;

        GsonUtil<Escena> gsonUtil = new GsonUtil();

    int n_escenas= MIN_NUM_ESCENAS+generador.nextInt(VARIACION_NUM_ESCENAS);
   
    try
    {
        for (int i=0; i<n_escenas; i++)
        {
            int tiempo= MIN_TIEMPO_GENERACION+generador.nextInt(VARIACION_TIEMPO_GENERACION);  
            try 
            {
                TimeUnit.SECONDS.sleep(tiempo);
            } 
            catch (InterruptedException ex) 
            {
                Logger.getLogger(Generador.class.getName()).log(Level.SEVERE, null, ex);
            }
            
            Escena e= new Escena();
            
            e.inicializaEscena();
            resultado.add(e);
            
            if (e.getPrioridad() == true)
            {                                
                               
                creador_mensajes = sesion.createProducer(destinos[0]);
                contenido = sesion.createTextMessage(gsonUtil.encode(e, Escena.class));                
                creador_mensajes.send(contenido);
                creador_mensajes.close();
               
            }else
            {          
             
                creador_mensajes = sesion.createProducer(destinos[1]);
                contenido = sesion.createTextMessage(gsonUtil.encode(e, Escena.class));
                creador_mensajes.send(contenido);
                creador_mensajes.close();
               
            }   
                        
        }
        
        this.barrier.await();
               
   }catch(InterruptedException e ){ 
        throw e;//System.out.println("Se ha interrumpido la ejecución de"+this.toString()+"");
   } finally {           
        return resultado;
   }   
  
}
    @Override
    public String toString(){
        return "Generador"+ cadena;
    }
    
}